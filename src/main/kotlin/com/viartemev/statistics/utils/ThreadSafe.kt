package com.viartemev.statistics.utils

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
annotation class ThreadSafe