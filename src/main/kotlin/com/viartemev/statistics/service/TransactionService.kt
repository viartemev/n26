package com.viartemev.statistics.service

import arrow.core.Either
import arrow.core.Either.Companion.left
import arrow.core.Either.Companion.right
import com.viartemev.statistics.service.domain.SummaryStatistic
import com.viartemev.statistics.service.domain.Transaction
import com.viartemev.statistics.service.domain.TransactionsPerSecond
import com.viartemev.statistics.service.exception.InvalidTransaction
import com.viartemev.statistics.service.exception.InvalidTransactionTime
import org.springframework.stereotype.Service
import java.time.Duration
import java.time.Instant

@Service
class TransactionService(private val storage: StatisticStorage) {

    fun addTransaction(transaction: Transaction, requestTime: Instant): Either<InvalidTransaction, Transaction> =
            validateTransaction(transaction, requestTime)
                    .map { storage.addTransaction(it) }

    fun getStatistics(requestTime: Instant): SummaryStatistic = storage
            .getStatistics()
            .asSequence()
            .filterIsInstance<TransactionsPerSecond>()
            .filter { requestTime.epochSecond - it.epochSecond <= 60 }
            .fold(SummaryStatistic()) { sumStat, statPerSecond -> sumStat.update(statPerSecond) }
            .apply { if (count != 0L) average = sum / count.toBigDecimal() }

    private fun validateTransaction(transaction: Transaction, requestTime: Instant): Either<InvalidTransaction, Transaction> =
            if (Duration.between(transaction.instant, requestTime).seconds !in 0..59) left(InvalidTransactionTime) else right(transaction)

    fun deleteTransactions() = storage.deleteTransactions()
}
