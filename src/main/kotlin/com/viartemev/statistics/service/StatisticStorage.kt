package com.viartemev.statistics.service

import com.viartemev.statistics.service.domain.EmptyStatisticPerSecond
import com.viartemev.statistics.service.domain.StatisticPerSecond
import com.viartemev.statistics.service.domain.Transaction
import com.viartemev.statistics.service.domain.TransactionsPerSecond
import com.viartemev.statistics.utils.ThreadSafe
import org.springframework.stereotype.Component
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.concurrent.read
import kotlin.concurrent.write

@Component
@ThreadSafe
class StatisticStorage {
    private val storage = Array<StatisticPerSecond>(60) { EmptyStatisticPerSecond }
    private val lock = ReentrantReadWriteLock(true)

    fun addTransaction(transaction: Transaction): Transaction {
        val transactionEpochSecond = transaction.instant.epochSecond
        val index = (transactionEpochSecond % 60).toInt()
        lock.write {
            val statistic = storage[index]
            when (statistic) {
                is EmptyStatisticPerSecond -> storage[index] = extractStatisticFromTransaction(transaction)
                is TransactionsPerSecond -> if (statistic.epochSecond != transactionEpochSecond) {
                    storage[index] = extractStatisticFromTransaction(transaction)
                } else statistic.update(transaction)
            }
        }
        return transaction
    }

    fun deleteTransactions() = lock.write { storage.fill(EmptyStatisticPerSecond) }

    fun getStatistics() = lock.read { storage.toList() }

    private fun extractStatisticFromTransaction(transaction: Transaction) = TransactionsPerSecond(
            epochSecond = transaction.instant.epochSecond,
            count = 1,
            sum = transaction.amount,
            max = transaction.amount,
            min = transaction.amount
    )
}