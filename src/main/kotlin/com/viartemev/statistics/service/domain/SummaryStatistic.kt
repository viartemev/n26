package com.viartemev.statistics.service.domain

import java.math.BigDecimal

data class SummaryStatistic(
        var count: Long = 0,
        var sum: BigDecimal = BigDecimal.ZERO,
        var average: BigDecimal = BigDecimal.ZERO,
        var max: BigDecimal? = null,
        var min: BigDecimal? = null) {

    fun update(transactionsPerSecond: TransactionsPerSecond): SummaryStatistic {
        sum += transactionsPerSecond.sum
        count += transactionsPerSecond.count
        min = if (min == null) transactionsPerSecond.min else transactionsPerSecond.min.min(min)
        max = if (max == null) transactionsPerSecond.max else transactionsPerSecond.max.max(max)
        return this
    }
}