package com.viartemev.statistics.service.domain

import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal
import java.time.Instant

data class Transaction(
        val amount: BigDecimal,
        @JsonProperty("timestamp")
        val instant: Instant
)