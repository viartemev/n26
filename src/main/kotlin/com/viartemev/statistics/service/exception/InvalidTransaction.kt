package com.viartemev.statistics.service.exception

sealed class InvalidTransaction(message: String) : RuntimeException(message)

object InvalidTransactionTime : InvalidTransaction("Invalid transaction time: duration between transaction time and current time")

