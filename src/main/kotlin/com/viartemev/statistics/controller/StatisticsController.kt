package com.viartemev.statistics.controller

import com.viartemev.statistics.service.TransactionService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.time.Instant

@RestController
class StatisticsController(private val transactionService: TransactionService) {

    @GetMapping("/statistics")
    fun getStatistics() = ResponseEntity.ok(transactionService.getStatistics(Instant.now()))

}