package com.viartemev.statistics.controller

import com.viartemev.statistics.service.TransactionService
import com.viartemev.statistics.service.domain.Transaction
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.time.Instant

@RestController
class TransactionController(private val transactionService: TransactionService) {

    @PostMapping("/transactions")
    fun addTransaction(@RequestBody transaction: Transaction) =
            transactionService.addTransaction(transaction, Instant.now())
                    .fold({ ResponseEntity.noContent().build<Nothing>() }, { trx -> ResponseEntity.ok(trx) })

    @DeleteMapping("/transactions")
    fun deleteTransactions(): ResponseEntity<Nothing> {
        transactionService.deleteTransactions()
        return ResponseEntity.noContent().build<Nothing>()
    }
}