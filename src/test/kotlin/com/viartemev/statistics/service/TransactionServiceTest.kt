package com.viartemev.statistics.service

import com.viartemev.statistics.service.domain.Transaction
import com.viartemev.statistics.service.exception.InvalidTransaction
import org.junit.Test
import java.math.BigDecimal
import java.time.Instant

class TransactionServiceTest {

    @Test(expected = InvalidTransaction::class)
    fun `Transaction service must decline transactions older than 60 seconds`() {
        val transactionService = TransactionService(StatisticStorage())
        val oldTransaction = Transaction(BigDecimal.TEN, Instant.now().minusSeconds(90))
        transactionService.addTransaction(oldTransaction)
    }

    @Test(expected = InvalidTransaction::class)
    fun `Transaction service must decline transactions newer than current time`() {
        val transactionService = TransactionService(StatisticStorage())
        val oldTransaction = Transaction(BigDecimal.TEN, Instant.now().plusSeconds(90))
        transactionService.addTransaction(oldTransaction)
    }

    @Test
    fun `Transaction service must add correct transaction to the statistics storage`() {

    }

}