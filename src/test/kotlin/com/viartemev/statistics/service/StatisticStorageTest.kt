package com.viartemev.statistics.service

import com.viartemev.statistics.service.domain.EmptyStatisticPerSecond
import com.viartemev.statistics.service.domain.Transaction
import com.viartemev.statistics.service.domain.TransactionsPerSecond
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.math.BigDecimal
import java.time.Instant

class StatisticStorageTest {

    @Test
    fun `The storage was initialized with empty statistic objects`() {
        val storage = StatisticStorage()
        assertThat(storage.getStatistics()).hasSize(60)
        assertThat(storage.getStatistics()).containsOnly(EmptyStatisticPerSecond)
    }

    @Test
    fun `The storage must return immutable list of statistics`() {
        val storage = StatisticStorage()
        assertThat(storage.getStatistics()).isInstanceOf(List::class.java)
    }

    @Test
    fun `The storage must replace old transactions statistic by new one if 60 seconds left`() {
        val storage = StatisticStorage()

        val curTrxAmount = BigDecimal.TEN
        val curTrxInstant = Instant.now()
        val curTrx = Transaction(curTrxAmount, curTrxInstant)

        val oldTrxAmount = BigDecimal.ONE
        val oldTrxInstant = curTrxInstant.minusSeconds(60)
        val oldTrx = Transaction(oldTrxAmount, oldTrxInstant)

        storage.addTransaction(oldTrx)
        storage.addTransaction(curTrx)

        val statisticForCurrentTime = TransactionsPerSecond(curTrxInstant.epochSecond, 1, curTrxAmount, curTrxAmount, curTrxAmount)
        val statisticFor60SecondsLeft = TransactionsPerSecond(oldTrxInstant.epochSecond, 1, oldTrxAmount, oldTrxAmount, oldTrxAmount)

        assertThat(storage.getStatistics()).containsOnlyOnce(statisticForCurrentTime)
        assertThat(storage.getStatistics()).doesNotContain(statisticFor60SecondsLeft)
    }

    @Test
    fun `The storage must update transaction statistics if transaction with the same time added`() {
        val storage = StatisticStorage()

        val curTrxAmount = BigDecimal.TEN
        val curTrxInstant = Instant.now()
        val curTrx = Transaction(curTrxAmount, curTrxInstant)

        val oldTrxAmount = BigDecimal.ONE
        val oldTrxInstant = curTrxInstant.minusMillis(60)
        val oldTrx = Transaction(oldTrxAmount, oldTrxInstant)

        storage.addTransaction(oldTrx)
        storage.addTransaction(curTrx)

        val statistic = TransactionsPerSecond(curTrxInstant.epochSecond, 2, curTrxAmount + oldTrxAmount, curTrxAmount, oldTrxAmount)

        assertThat(storage.getStatistics()).containsOnlyOnce(statistic)
    }


    @Test
    fun `The storage must filled by EmptyStatisticPerSecond on deleteTransactions method`() {
        val storage = StatisticStorage()
        storage.addTransaction(Transaction(BigDecimal.ONE, Instant.now()))
        assertThat(storage.getStatistics()).hasAtLeastOneElementOfType(TransactionsPerSecond::class.java)
        storage.deleteTransactions()
        assertThat(storage.getStatistics()).containsOnly(EmptyStatisticPerSecond)
    }
}